<?php 
	require_once "conexion.php";

	class ModeloUsuarios{

		static public function mdlMostrarUsuarios($tabla,$item,$valor){
			if ($item!=null) {
				$consulta=Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item=:$item");
				$consulta->bindParam(":".$item,$valor,PDO::PARAM_STR);
				$consulta->execute();
				return $consulta->fetch();
				
			}else{

				$consulta=Conexion::conectar()->prepare("SELECT * FROM $tabla");
				$consulta->execute();
				return $consulta->fetchAll();

			}

		}


		static public function mdlIngresarUsuario($tabla,$datos){
			$consulta=Conexion::conectar()->prepare("INSERT INTO $tabla(nombre,apellidos,usuario,password,perfil,foto) VALUES(:nombre,:apellidos,:usuario,:password,:perfil,:foto)");
			$consulta->bindParam(":nombre",$datos["nombre"],PDO::PARAM_STR);
			$consulta->bindParam(":usuario",$datos["usuario"],PDO::PARAM_STR);
			$consulta->bindParam(":apellidos",$datos["apellidos"],PDO::PARAM_STR);
			$consulta->bindParam(":password",$datos["password"],PDO::PARAM_STR);
			$consulta->bindParam(":perfil",$datos["perfil"],PDO::PARAM_STR);
			$consulta->bindParam(":foto",$datos["foto"],PDO::PARAM_STR);

			if ($consulta->execute()) {
				return "ok";
			}else{
				return "error";
			}
			
			$consulta->close();
			$consulta=null;
			
		}
	}

 ?>